<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\site\common\models\Page */

$this->title = Yii::t('site', 'Create Page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

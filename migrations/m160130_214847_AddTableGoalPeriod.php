<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160130_214847_AddTableGoalPeriod extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('goals_period', [
            'id'         => $this->primaryKey(11),
            'goal_id'    => $this->integer(11)->notNull(),
            'type'       => $this->smallInteger(1)->notNull(),
            'value'      => $this->smallInteger(2)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->createIndex('goals_period_link', 'goals_period', ['goal_id']);
        $this->createIndex('goals_period_value', 'goals_period', ['type', 'value']);

        $this->addForeignKey(
            'fk_goals_period_goal_id',
            'goals_period',
            'goal_id',
            'goals_goal',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_period_created_by',
            'goals_period',
            'created_by',
            'users_user',
            'id'
        );

        $this->createFuntionDow();
        $this->addPeriodTypes();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('goals_period');

        $this->afterMigrate();
    }

    private function createFuntionDow()
    {
        $sql = <<<SQL
CREATE OR REPLACE FUNCTION dayofweek(DATE)
  RETURNS INT AS $$
SELECT EXTRACT(ISODOW FROM $1) :: INT
$$
LANGUAGE SQL;
SQL;
        $this->db->createCommand($sql)->execute();
    }

    private function addPeriodTypes()
    {
        Variety::add('goal_period_type', 'goal_period_type_week', 'Дни недели', 1, 1);
        Variety::add('goal_period_type', 'goal_period_type_day', 'Дни месяца', 2, 2);
        Variety::add('goal_period_type', 'goal_period_type_month', 'Месяца', 3, 3);
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}

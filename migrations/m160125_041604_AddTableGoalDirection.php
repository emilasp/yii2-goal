<?php

use emilasp\goal\common\models\GoalDirection;
use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160125_041604_AddTableGoalDirection extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('goals_direction', [
            'id'         => $this->primaryKey(11),
            'name'       => $this->string(255)->notNull(),
            'text'       => $this->text(),
            'image_id'   => $this->integer(),
            'status'     => $this->smallInteger(1)->notNull(),
            'standart'   => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->createIndex('goals_direction_status', 'goals_direction', ['status']);
        $this->createIndex('goals_direction_standart', 'goals_direction', ['standart']);

        $this->addForeignKey(
            'fk_goals_direction_image_id',
            'goals_direction',
            'image_id',
            'files_file',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_direction_created_by',
            'goals_direction',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_direction_updated_by',
            'goals_direction',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addStandartDirections();

        $this->afterMigrate();
    }

    private function addStandartDirections()
    {
        $standarts = [
            ['name' => 'Здоровье', 'text' => 'Питание, спорт, привычки'],
            ['name' => 'Личностный рост', 'text' => 'Образование и личностный рост'],
            ['name' => 'Духовный рост', 'text' => 'Внутренний мир, духовность'],
            ['name' => 'Отношения', 'text' => 'Окружение'],
            ['name' => 'Работа', 'text' => 'Бизнес, карьера, работа'],
            ['name' => 'Финансы', 'text' => 'Деньги и финансы'],
            ['name' => 'Яркость жизни', 'text' => 'Окраска жизни, степень её яркости'],
        ];

        foreach ($standarts as $standart) {
            $model           = new GoalDirection($standart);
            $model->status   = 1;
            $model->standart = 1;
            $model->save();
        }

        $this->db->createCommand("UPDATE goals_direction SET created_by=1, updated_by=1")->execute();
    }

    public function down()
    {
        $this->dropTable('goals_direction');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}

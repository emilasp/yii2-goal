<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160125_041621_AddTableGoal extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('goals_goal', [
            'id'            => $this->primaryKey(11),
            'name'          => $this->string(255)->notNull(),
            'text'          => $this->text(),
            'direction_id'  => $this->integer()->notNull(),
            'project_id'    => $this->integer(),
            'unit'          => $this->string(15),
            'unit_step'     => $this->integer(),
            'result_expect' => $this->integer(),
            'result_actual' => $this->integer(),
            'date_start'    => $this->dateTime(),
            'date_end'      => $this->dateTime(),
            'time'          => $this->time(),
            'type'          => $this->smallInteger(1)->notNull(),
            'type_unit'     => $this->smallInteger(1)->notNull(),
            'public'        => $this->smallInteger(1)->notNull(),
            'status'        => $this->smallInteger(1)->notNull(),
            'created_at'    => $this->dateTime(),
            'updated_at'    => $this->dateTime(),
            'created_by'    => $this->integer(11),
            'updated_by'    => $this->integer(11),
        ], $this->tableOptions);

        $this->createIndex('goals_goal_status_public', 'goals_goal', ['status', 'public']);

        $this->addForeignKey(
            'fk_goals_goal_direction_id',
            'goals_goal',
            'direction_id',
            'goals_direction',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_goal_project_id',
            'goals_goal',
            'project_id',
            'goals_project',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_goal_created_by',
            'goals_goal',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_goal_updated_by',
            'goals_goal',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addGoalTypes();
        $this->addGoalUnitType();
        $this->addGoalStatuses();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('goals_goal');

        $this->afterMigrate();
    }

    private function addGoalTypes()
    {
        Variety::add('goal_type', 'goal_type_goal', 'Цель', 1, 1);
        Variety::add('goal_type', 'goal_type_goal_unlimited', 'Цель(без результата)', 2, 2);
        Variety::add('goal_type', 'goal_type_habit', 'Привычка', 3, 3);
        Variety::add('goal_type', 'goal_type_habit_unlimited', 'Привычка(без результата)', 4, 4);
        Variety::add('goal_type', 'goal_type_habit_bad', 'Плохая привычка', 5, 5);
    }

    private function addGoalUnitType()
    {
        Variety::add('goal_unit', 'goal_unit_simple', 'Простое', 1, 1);
        Variety::add('goal_unit', 'goal_unit_time', 'Время', 2, 2);
        Variety::add('goal_unit', 'goal_unit_custom', 'Пользовательское', 3, 3);
    }

    private function addGoalStatuses()
    {
        Variety::add('goal_status', 'goal_status_fail', 'Провалена', 0, 0);
        Variety::add('goal_status', 'goal_status_completed', 'Завершена', 1, 1);
        Variety::add('goal_status', 'goal_status_work', 'В работе', 2, 2);
        Variety::add('goal_status', 'goal_status_pause', 'На паузе', 3, 3);
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}

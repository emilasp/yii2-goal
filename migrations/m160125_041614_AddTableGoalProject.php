<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160125_041614_AddTableGoalProject extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    public function up()
    {
        $this->createTable('goals_project', [
            'id'           => $this->primaryKey(11),
            'name'         => $this->string(255)->notNull(),
            'text'         => $this->text(),
            'direction_id' => $this->integer()->notNull(),
            'image_id'     => $this->integer(),
            'status'       => $this->smallInteger(1)->notNull(),
            'created_at'   => $this->dateTime(),
            'updated_at'   => $this->dateTime(),
            'created_by'   => $this->integer(11),
            'updated_by'   => $this->integer(11),
        ], $this->tableOptions);

        $this->createIndex('goals_project_status', 'goals_project', ['status']);

        $this->addForeignKey(
            'fk_goals_project_direction_id',
            'goals_project',
            'direction_id',
            'goals_direction',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_project_image_id',
            'goals_project',
            'image_id',
            'files_file',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_project_created_by',
            'goals_project',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_goals_project_updated_by',
            'goals_project',
            'updated_by',
            'users_user',
            'id'
        );

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('goals_project');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}

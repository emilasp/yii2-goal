<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
?>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('goal', 'Goal CheckList') ?></h3>

        <div class="panel-options">
            <?= Html::a(
                Yii::t('goal', 'Create Goal'),
                ['/goal/goal/create'],
                ['class' => 'btn btn-success btn-xs', 'data-pjax' => 0]
            ) ?>
        </div>
    </div>
    <div class="panel-body">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions'  => ['class' => 'item'],
            'itemView'     => function ($model, $key, $index, $widget) use ($day, $now) {
                return $this->render('rows/check-goal', [
                    'model'        => $model,
                    'index'        => $index,
                    'now'          => $now,
                    'day'          => $day,
                ]);
            },
        ]) ?>
    </div>
</div>

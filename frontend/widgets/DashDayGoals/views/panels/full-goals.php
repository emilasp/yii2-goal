<?php
use emilasp\goal\common\models\GoalResult;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

?>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('goal', 'Goal goals') ?></h3>

        <div class="panel-options">
            <?= Html::a(
                Yii::t('goal', 'Create Goal'),
                ['/goal/goal/create'],
                ['class' => 'btn btn-success btn-xs', 'data-pjax' => 0]
            ) ?>
        </div>
    </div>
    <div class="panel-body">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions'  => ['class' => 'item'],
            'itemView'     => function ($model, $key, $index, $widget) use ($day, $now) {
                $results = $model->getAllResultsByDate();

                $dataForGraph = ['values' => []];
                foreach ($results as $date => $result) {
                    $dataForGraph['values'][] = [
                        'X' => $date,
                        'Y' => $result,
                    ];
                }
                return $this->render('rows/full-goal', [
                    'model'        => $model,
                    'index'        => $index,
                    'now'          => $now,
                    'day'          => $day,
                    'dataForGraph' => $dataForGraph,
                ]);
            },
        ]) ?>
    </div>
</div>

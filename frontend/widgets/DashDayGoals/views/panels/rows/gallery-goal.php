<?php
use emilasp\files\models\File;
?>

<img src="<?= $model->image->getUrl(File::SIZE_MIN) ?>" width="100%"/>
<div>
    <?php foreach ($model->files as $image) : ?>
        <a class="goald-day-gallery-image" href="<?= $image->getUrl(File::SIZE_MAX) ?>" data-jbox-image="gallery1" data-pjax="0">
            <img src="<?= $image->getUrl(File::SIZE_ICO) ?>"  height="30px"/>
        </a>
    <?php endforeach ?>
</div>
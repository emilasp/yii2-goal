<?php
use emilasp\files\models\File;
use emilasp\goal\common\models\GoalResult;
use yii\helpers\Html;
use yii\helpers\Url;

$statistic = $model->getResultStatistic();
$result    = $model->getResultOfDay($day);

$rowClass = '';
$checked = '';
$noResult = '';
if ($result) {
    if ($result->status === GoalResult::STATUS_ENABLED) {
        $rowClass = 'goal-result-row-success';
        $checked = 'checked="checked"';
    } else {
        $rowClass = 'goal-result-row-unsuccess';
    }
} else {
    if ($day < $now) {
        $rowClass = 'goal-result-row-unsuccess';
    }
    $noResult = 'no-result';
}
?>
    <div class="clearfix dash-goal-main-row position-relative">
        <canvas id="graph-day-<?= $model->id ?>" width="500" height="50"></canvas>

        <div class="dash-day-row-goal <?= $rowClass ?>"
             data-id="<?= $model->id ?>" data-type="<?= $model->type ?>"
             data-type-unit="<?= $model->type_unit ?>" data-unit="<?= $model->unit ?>"
             data-unit-step="<?= $model->unit_step ?>">

            <div class="dash-day-goal-img">
                <a href="<?= $model->image->getUrl(File::SIZE_MAX) ?>" data-jbox-image="gallery2" data-pjax="0">
                    <img src="<?= $model->image->getUrl(File::SIZE_ICO) ?>" height="30px"/>
                </a>
            </div>

            <div class="dash-day-goal-checked">
                <div class="row">
                    <div class="col-md-12">
                        <div class="onoffswitch">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox <?= $day < $now ? 'noNow' : '' ?>" id="myonoffswitch"
                                <?= $checked ?>>
                            <label class="onoffswitch-label" for="myonoffswitch">
                                <span class="onoffswitch-inner <?= $noResult ?>"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="dash-day-goal-name">
                <?= $model->name ?>
            </div>
            <div class="dash-day-goal-description">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-info btn-sm goal-info-button">
                            <i class="fa fa-info"></i>
                        </button>
                    </div>
                </div>

            </div>

        </div>
        <div>
            <div class="progressbar">
                <div style="width: <?= $statistic['percent'] ?>%"></div>
            </div>
            <div class="progressbar-light">
                <div style="width: <?= $statistic['percentCount'] ?>%"></div>
            </div>
        </div>
        <div class="dash-day-row-goal-info">
            <div class="row">
                <div class="col-md-4">
                    <?= $this->render('gallery-goal', ['model' => $model]) ?>
                    <div class="clearfix">
                        <strong><?= Yii::t('goal', 'Days') ?>:</strong> - <?= $statistic['days'] ?>,
                        <strong><?= Yii::t('goal', 'Results') ?>:</strong> - <?= $statistic['results'] ?>,
                        <strong><?= Yii::t('goal', 'Fails') ?>:</strong> - <?= $statistic['fails'] ?>,
                        <strong><?= Yii::t('goal', 'Continue') ?>:</strong> - <?= $statistic['continue'] ?>,
                        <strong><?= Yii::t('goal', 'Max') ?>:</strong> - <?= $statistic['maxContinue'] ?>,
                        <strong><?= Yii::t('goal', 'Future') ?>:</strong> - <?= $statistic['future'] ?>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="clearfix"><?= $model->text ?></div>

                    <?php if ($result) : ?>
                        <strong><?= Yii::t('goal', 'Result') ?></strong>
                        <div class="row">
                            <div class="col-md-6">
                                <strong><?= Yii::t('goal', 'Result time') ?>:</strong>
                            </div>
                            <div class="col-md-6">
                                <?= date('H:i:s', strtotime($result->created_at)) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <strong><?= Yii::t('goal', 'Start date') ?>:</strong>
                            </div>
                            <div class="col-md-6">
                                <?= $model->date_start ? date('d.m.Y', strtotime($model->date_start)) : '-' ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <strong><?= Yii::t('goal', 'End date') ?>:</strong>
                            </div>
                            <div class="col-md-6">
                                <?= $model->date_end ? date('d.m.Y', strtotime($model->date_end)) : '-' ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <strong><?= Yii::t('goal', 'Result') ?>:</strong>
                            </div>
                            <div class="col-md-6">
                                <?= $result->result ?> <?= $model->unit ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <strong><?= Yii::t('goal', 'Result comment') ?>:</strong><br/>

                                <?= $result->text ?>
                            </div>
                        </div>

                    <?php endif ?>
                </div>
            </div>
        </div>

        <div class="dash-day-row-goal-result">
            <div class="row">
                <div class="col-md-4">
                    <?= $this->render('gallery-goal', ['model' => $model]) ?>
                </div>
                <div class="col-md-8">

                    <div class="modal-body">
                        <label class="goalResultResultLabel"> <?= Yii::t('goal', 'Result value') ?></label>

                        <div class="row">
                            <input type="hidden" class="goalResultId" value="<?= $model->id ?>"/>
                            <input type="hidden" class="goalResultSuccess"/>

                            <div class="col-md-12">
                                <?= Html::input(
                                    'number',
                                    'goalResult[value]',
                                    ($result ? $result->result : ''),
                                    [
                                        'class' => 'goalResultResult range range-gold form-control text-right',
                                        'min'   => '0',
                                    ]
                                ) ?>
                            </div>
                        </div>
                        <label> <?= Yii::t('goal', 'Result comment') ?></label>

                        <div class="row">
                            <div class="col-md-12">
                                <?= Html::textarea(
                                    'goalResult[comment]',
                                    ($result ? $result->text : ''),
                                    [
                                        'class' => 'goalResultComment form-control',
                                        'rows'  => '5',
                                    ]
                                ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default close-goal-result">
                            <?= Yii::t('site', 'Close') ?>
                        </button>
                        <button type="button" class="btn btn-primary sendGoalResult">
                            <?= Yii::t('goal', 'Save result') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$js = <<<JS

JS;

$this->registerJs($js);

<?php
use emilasp\goal\common\models\GoalResult;
use emilasp\goal\frontend\widgets\DashDayGoals\DashDayGoalsWeek;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;

?>

<?php Pjax::begin(['id' => 'dashGoalWeekCur']) ?>
    <div class="dashGoalWeekMain clearfix">
        <?php foreach ($datesWithGoals as $index => $weekDay) : ?>
            <div class="dashGoalWeekday
             <?= $index > 4 ? 'red' : '' ?>
             <?= $weekDay['date'] == $now ? 'now' : '' ?>
             <?= $weekDay['date'] == $day ? 'curdate' : '' ?>
             ">
            <a href="<?= Url::toRoute(['/goal/goal-dashboard', 'day' => $weekDay['date']]) ?>" data-pjax="0">
                <div class="dashGoalWeekDay">
                    <div class="float-left">
                        <strong><?= Yii::t('site', DashDayGoalsWeek::$weekdays[$index]) ?></strong>
                    </div>
                    <div class="float-right">
                        <small><?= date('m.d', strtotime($weekDay['date'])) ?></small>
                    </div>
                </div>
            </a>
                <div class="dashGoalWeekGoals">
                    <?php foreach ($weekDay['goals'] as $typeResult => $goals) : ?>
                        <?php foreach ($goals as $goal) : ?>
                            <div class="dashGoalWeekGoalRow result-<?= $typeResult ?> clearfix">
                                <div class="dashGoalWeekDayName float-left">
                                    <?= $goal->name ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endforeach ?>
                </div>

            </div>

        <?php endforeach ?>
    </div>


<?php Pjax::end() ?>

<?php
$urlFoResult = Url::toRoute(['/goal/goal-dashboard', 'day' => $day]);

/** @var \yii\web\View $this */

$js = <<<JS

JS;


$this->registerJs($js);

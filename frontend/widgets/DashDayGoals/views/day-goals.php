<?php
use yii\widgets\Pjax;

?>

<?php Pjax::begin(['id' => 'dashGoalGridCur']) ?>

<div class="row">
    <div class="col-md-7">
        <?= $this->render('panels/full-goals', [
            'day'          => $day,
            'now'          => $now,
            'dataProvider' => $dataProviderFull,
        ]) ?>
    </div>
    <div class="col-md-5">
        <?= $this->render('panels/checklist-goals', [
            'day'          => $day,
            'now'          => $now,
            'dataProvider' => $dataProviderCheckList,
        ]) ?>
    </div>
</div>

<?php Pjax::end() ?>

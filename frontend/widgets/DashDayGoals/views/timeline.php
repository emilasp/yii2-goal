<?php
use yii\helpers\Url;
?>

<div class="goalTimelineBlock">
    <?php foreach ($dates as $date => $params) : ?>
    <a href="<?= Url::toRoute(['/goal/goal-dashboard', 'day' => $date]) ?>"
       class="goalTimelineDay <?= $params['active'] ? 'active' : '' ?> <?= $params['now'] ? 'now' : '' ?>
        <?= $params['weekday'] > 5 ? 'weekend' : '' ?>">
        <?= date('d', strtotime($date)) ?>
    </a>
    <?php endforeach ?>
</div>



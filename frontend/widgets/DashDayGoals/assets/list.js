DrawChart = function(selector, data) {
    var graph;
    var context;
    this.xPadding = 10;
    this.yPadding = 10;
    this.maxY = 0;
    var obj = this;

    this.setMaxY = function() {
        var max = 0;
        for(var i = 0; i < obj.data.values.length; i ++) {
            if(obj.data.values[i].Y > max) {
                max = obj.data.values[i].Y;
            }
        }
        max += 10 - max % 10;
        this.maxY = max;
    }

    this.getXPixel = function(val) {
        return ((obj.graph.width() - obj.xPadding) / obj.data.values.length) * val - 50;
    }

    this.getYPixel = function(val) {
        return obj.graph.height() - (((obj.graph.height() - obj.yPadding) / obj.maxY) * val) - obj.yPadding;
    }

    this.init = function(selector, data) {
        obj.data = data;
        obj.graph = $(selector);
        obj.cntxt = obj.graph[0].getContext('2d');
        obj.cntxt.lineWidth = 1;
        obj.cntxt.strokeStyle = '#333';
        obj.cntxt.font = 'italic 8pt sans-serif';
        obj.cntxt.textAlign = "center";

        this.setMaxY();



        //obj.drawAxis();
        obj.drawDays();
        obj.drawChart();
        obj.drawChartPoints();

    }

    this.drawAxis = function() {
        obj.cntxt.beginPath();
        obj.cntxt.moveTo(obj.xPadding, 0);
        obj.cntxt.lineTo(obj.xPadding, obj.graph.height() - obj.yPadding);
        obj.cntxt.lineTo(obj.graph.width(), obj.graph.height() - obj.yPadding);
        obj.cntxt.stroke();

        for(var i = 0; i < obj.data.values.length; i ++) {
            obj.cntxt.fillText(obj.data.values[i].X, obj.getXPixel(i), obj.graph.height() - obj.yPadding + 20);
        }
    }

    this.drawDays = function() {
        obj.cntxt.textAlign = "right"
        obj.cntxt.textBaseline = "middle";
        for(var i = 0; i < obj.maxY; i += 5) {
            obj.cntxt.fillText(i, obj.xPadding - 10, obj.getYPixel(i));
        }
    }

    this.drawChart = function() {
        var firstY = data.values.length ? data.values[0].Y : 0;

        obj.cntxt.beginPath();
        obj.cntxt.moveTo(obj.getXPixel(0), obj.getYPixel(firstY));
        obj.cntxt.stroke();
        var oldValue = firstY;
        for(var i = 1; i < obj.data.values.length; i ++) {
            if (oldValue > obj.data.values[i].Y) {
                obj.cntxt.strokeStyle = '#f00';
            } else {
                obj.cntxt.strokeStyle = '#008A00';
            }

            obj.cntxt.lineTo(obj.getXPixel(i), obj.getYPixel(obj.data.values[i].Y));
            obj.cntxt.stroke();
            oldValue = obj.data.values[i].Y;
        }
    }

    this.drawChartPoints = function() {
        obj.cntxt.fillStyle = '#333';

        var maxY = obj.maxY;
        for(var i = 0; i < obj.data.values.length; i ++) {
            if (maxY/2 > obj.data.values[i].Y) {
                obj.cntxt.fillStyle = '#B11B1B';
            } else {
                obj.cntxt.fillStyle = '#11442A';
            }

            obj.cntxt.beginPath();

            obj.cntxt.arc(obj.getXPixel(i), obj.getYPixel(obj.data.values[i].Y), 2, 0, Math.PI * 2, true);
            obj.cntxt.fill();
        }
    }

    this.init(selector, data);
}

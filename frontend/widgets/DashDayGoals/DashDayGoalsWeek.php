<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\goal\frontend\widgets\DashDayGoals;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use emilasp\goal\common\models\GoalDirection;
use emilasp\goal\common\models\GoalProject;
use emilasp\goal\common\models\GoalResult;
use yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;

/**
 * Class DashDayGoalsWeek
 * @package emilasp\goal\frontend\widgets\DashDayGoals
 */
class DashDayGoalsWeek extends Widget
{
    public $pageSizes = 10;

    private $curDate;
    private $startDate;

    static public $weekdays = [
        0 => 'mo',
        1 => 'tu',
        2 => 'we',
        3 => 'th',
        4 => 'fr',
        5 => 'sa',
        6 => 'su',
    ];

    public function init()
    {
        $this->registerAssets();

        $this->curDate   = Yii::$app->request->get('day', date('Y-m-d'));
        $this->startDate = date('Y-m-d', strtotime('last Monday', strtotime($this->curDate . ' tomorrow')));
    }

    public function run()
    {
        $datesWithGoals = $this->getGoalsByDays();

        echo $this->render('week-goals', [
            'day'            => $this->curDate,
            'now'            => date('Y-m-d'),
            'datesWithGoals' => $datesWithGoals,
        ]);
    }

    /** Получаем даты с целями
     * @return array
     */
    private function getGoalsByDays()
    {
        $dates = [];
        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', strtotime($this->startDate . ' +' . $i . ' days'));

            $dates[$i]['date']  = $date;
            $dates[$i]['goals'] = $this->sortGoalsInDay(Goal::getGoalsByDate($date), $date);
        }
        return $dates;
    }

    /** Сортируем цели(сперва идут без результата, выполненные, не выполненные)
     *
     * @param Goal[] $goals
     * @param $date
     *
     * @return array
     */
    private function sortGoalsInDay($goals, $date)
    {
        $goalsByResult = [
            'info'    => [],
            'success' => [],
            'danger'  => [],
        ];
        foreach ($goals as $goal) {
            $result = $goal->getResultOfDay($date);
            if ($result && !$result->status) {
                $goalsByResult['danger'][] = $goal;
            } elseif ($result && $result->status) {
                $goalsByResult['success'][] = $goal;
            } else {
                $goalsByResult['info'][] = $goal;
            }
        }
        return $goalsByResult;
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        DashDayGoalsAsset::register($view);
    }
}

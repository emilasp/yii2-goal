<?php
namespace emilasp\goal\frontend\widgets\DashDayGoals;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use emilasp\goal\common\models\GoalResult;
use yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * Class DashDayGoals
 * @package emilasp\goal\frontend\widgets\DashDayGoals
 */
class DashDayGoals extends Widget
{
    public $pageSizes = 10;

    private $curDate;

    public function init()
    {
        $this->registerAssets();
        $this->curDate = Yii::$app->request->get('day', date('Y-m-d'));

        $this->saveResult();
    }

    public function run()
    {
        $dataProviders = $this->getDataProviders();

        echo $this->render('day-goals', [
            'day'                   => $this->curDate,
            'now'                   => date('Y-m-d'),
            'dataProviderFull'      => $dataProviders['full'],
            'dataProviderCheckList' => $dataProviders['simple'],
        ]);
    }

    /** Формируем датапровайдеры
     * @return array
     */
    private function getDataProviders()
    {
        $models = Goal::getGoalsByDate($this->curDate);

        $simpleModels = [];
        $fullModels   = [];

        foreach ($models as $model) {
            if ($model->type_unit === Goal::UNIT_TYPE_SIMPLE) {
                $simpleModels[] = $model;
            } else {
                $fullModels[] = $model;
            }
        }

        return [
            'full'   => new ArrayDataProvider([
                'allModels'  => $fullModels,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'simple' => new ArrayDataProvider([
                'allModels'  => $simpleModels,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
        ];
    }

    /**
     * Сохраняем результат
     */
    private function saveResult()
    {
        if ($data = Yii::$app->request->post('goalResult')) {
            GoalResult::addResult($this->curDate, $data['goal_id'], $data['status'], $data['result'], $data['text']);
        }
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        DashDayGoalsAsset::register($view);

        $urlFoResult = Url::toRoute(['/goal/goal-dashboard', 'day' => $this->curDate]);
        $js          = <<<JS
    $(document).ready(function(){
        new jBox('Image');

        $(document).on('click', '.onoffswitch-checkbox', function () {
            var button = $(this);
            var mainBlock = button.closest('.dash-goal-main-row');
            var dataBlock = mainBlock.find('.dash-day-row-goal');
            var resultBlock = mainBlock.find('.dash-day-row-goal-result');

            var success = button.is(':checked');
            var noNow = button.hasClass('noNow');
            var id = dataBlock.data('id');
            var type = dataBlock.data('type');

            mainBlock.find('.goalResultId').val(id);
            mainBlock.find('.goalResultSuccess').val(success ? 1 : 0);

            resultBlock.find('.goalResultResult').val(success ? 1 : 0);
            if (!noNow) {
                resultBlock.find('.sendGoalResult').click();
            }
        });

        $(document).on('click', '.accept-goal, .accept-goal-cancel', function () {
            $('.dash-day-row-goal-result, .dash-day-row-goal-info').slideUp();

            var button = $(this);
            var mainBlock = button.closest('.dash-goal-main-row');
            var dataBlock = mainBlock.find('.dash-day-row-goal');
            var resultBlock = mainBlock.find('.dash-day-row-goal-result');


            resultBlock.slideUp("fast", function () {
                var success = button.hasClass('accept-goal');
                var id = dataBlock.data('id');
                var type = dataBlock.data('type');

                var unitStep = dataBlock.data('unit-step');

                mainBlock.find('.goalResultId').val(id);
                mainBlock.find('.goalResultSuccess').val(success ? 1 : 0);
                mainBlock.find('.goalResultResult').attr('step', unitStep);

                resultBlock.removeClass('result-success');
                resultBlock.removeClass('result-danger');
                mainBlock.find('.goalResultResult, .goalResultResultLabel').removeClass('hidden');
                resultBlock.find('.sendGoalResult').removeClass('btn-primary').removeClass('btn-danger').removeClass('btn-success');

                if (success) {
                   resultBlock.addClass('result-success');
                   resultBlock.find('.sendGoalResult').addClass('btn-success');
                } else {
                   resultBlock.addClass('result-danger');
                   resultBlock.find('.sendGoalResult').addClass('btn-danger');
                   mainBlock.find('.goalResultResult, .goalResultResultLabel').addClass('hidden');
                }

                resultBlock.slideDown();
            });
        });

        $(document).on('click', '.sendGoalResult', function () {
            var button = $(this);

            var mainBlock = button.closest('.dash-goal-main-row');
            var resultBlock = mainBlock.find('.dash-day-row-goal-result');

            var id = resultBlock.find('.goalResultId').val();
            var success = resultBlock.find('.goalResultSuccess').val();
            var result = resultBlock.find('.goalResultResult').val();
            var comment = resultBlock.find('.goalResultComment').val();

            mainBlock.find('.dash-day-row-goal').loading(true);

            $.pjax({
                type: 'POST',
                url: "{$urlFoResult}",
                timeout : 0,
                container:"#dashGoalGridCur",
                push:false,
                scrollTo:false,
                data:{
                    "goalResult[goal_id]":id,
                    "goalResult[status]":success,
                    "goalResult[result]":result,
                    "goalResult[text]":comment
                }
            });

            resultBlock.slideUp();
        });

        $('body').on("pjax:end", "#dashGoalGridCur", function() {
              notice('Goal result set', 'green');
              $.pjax({
                url: "{$urlFoResult}",
                timeout : 0,
                container:"#dashGoalWeekCur",
                push:false,
                scrollTo:false
            });
         });

        $(document).on('click', '.goal-info-button', function () {
            $('.dash-day-row-goal-result').slideUp();
            var blockInfo = $(this).closest('.dash-goal-main-row').find('.dash-day-row-goal-info');
            blockInfo.slideToggle("slow");
        });
        $(document).on('click', '.close-goal-result', function () {
            var button = $(this);

            var mainBlock = button.closest('.dash-goal-main-row');
            var resultBlock = mainBlock.find('.dash-day-row-goal-result');
            resultBlock.slideUp();
        });
    });
JS;


        $view->registerJs($js);
    }
}

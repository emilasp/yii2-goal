<?php

namespace emilasp\goal\frontend\widgets\DashDayGoals;

use emilasp\core\components\base\AssetBundle;

/**
 * Class GoalListAsset
 * @package emilasp\goal\frontend\widgets\DashDayGoals
 */
class DashDayGoalsAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'list'
    ];
    public $js = [
        'list'
    ];
}

<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\goal\frontend\widgets\DashDayGoals;

use emilasp\core\components\base\Widget;
use emilasp\core\helpers\DateHelper;
use emilasp\goal\common\models\Goal;
use emilasp\goal\common\models\GoalDirection;
use emilasp\goal\common\models\GoalProject;
use yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;

/**
 * Class DashDayTimeline
 * @package emilasp\goal\frontend\widgets\DashDayGoals
 */
class DashDayTimeline extends Widget
{
    private $curDate;

    public function init()
    {
        $this->registerAssets();
        $this->curDate = Yii::$app->request->get('day', date('Y-m-d'));
    }

    public function run()
    {
        echo $this->render('timeline', ['dates' => $this->days]);

    }

    /** Получаем даты вокруг сегодня
     * @return array
     */
    public function getDays()
    {
        $start = date('Y-m-d H:i:s', strtotime($this->curDate . '-10 days'));
        $end   = date('Y-m-d H:i:s', strtotime($this->curDate . '+10 days'));
        $days  = DateHelper::getDatesByRange($start, $end);

        $result = [];
        $now    = date('Y-m-d');
        foreach ($days as $index => $day) {
            $time    = strtotime($day);
            $day     = date('Y-m-d', $time);
            $weekDay = date('N', $time);

            $result[$day]['active']  = $day === $this->curDate;
            $result[$day]['now']     = $day === $now;
            $result[$day]['weekday'] = $weekDay;
        }
        return $result;
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        DashDayGoalsAsset::register($view);
    }
}

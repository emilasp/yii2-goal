<?php
use dosamigos\chartjs\ChartJs;
use emilasp\core\helpers\DateHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>
<?php Pjax::begin(['id' => 'goal-calendar']); ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <?= Yii::t('goal', 'Calendar') ?>

            </h3>

            <div class="float-right">
                <a class="btn btn-info btn-xs"
                   href="<?= Url::toRoute(['/goal/goal-dashboard/calendar', 'date' => $prevMonth]) ?>"> << </a>
                <?= Yii::t('site', $curentMonth) ?>
                <a class="btn btn-info btn-xs"
                   href="<?= Url::toRoute(['/goal/goal-dashboard/calendar', 'date' => $nextMonth]) ?>">>></a>
            </div>
        </div>
        <div class="panel-body">

            <table class="goal-calendar-table" cellspacing="3">
                <tr>
                    <?php for ($i = 0; $i < 7; $i++) : ?>
                        <th class="goal-calendar-ceil <?= $i > 4 ? 'weekend' : '' ?>">
                            <?= Yii::t('goal', DateHelper::$weekDaysShortName[$i]) ?>
                        </th>
                    <?php endfor ?>
                </tr>

                <?php for ($i = 0; $i < count($data) / 7; $i++) : ?>
                    <tr>
                        <?php for ($k = 0; $k < 7; $k++) : ?>

                            <?php
                            $date  = $data[$i * 7 + $k]['date'];
                            $goals = $data[$i * 7 + $k]['goals'];

                            $inActive = ($dateStart > $date || $dateEnd < $date);
                            ?>

                            <td class="goal-calendar-ceil <?= $inActive ? 'disabled-day' : '' ?>  <?= $k > 4 ? 'weekend' : '' ?> <?= $nowDate === $date ? 'now' : '' ?>">
                                <div class="goal-calendar-date"><?= date('d', strtotime($date)) ?></div>
                                <div>
                                    <?php
                                    $success = 0;
                                    $fail    = 0;
                                    $all     = 0;
                                    ?>
                                    <?php foreach ($goals as $goal) : ?>
                                        <?php
                                        if ($result = $goal->getResultOfDay($date)) {
                                            if ($result->status) {
                                                ?>
                                                <div class="goal-calendar-result-quadro success">
                                                    <?= $result->result ?>
                                                </div>
                                                <?php
                                                $success++;
                                            } else {
                                                ?>
                                                <div class="goal-calendar-result-quadro fail">x</div>
                                                <?php
                                                $fail++;
                                            }
                                        } else {
                                            ?>
                                            <div class="goal-calendar-result-quadro new">?</div>
                                            <?php
                                        }
                                        $all++;
                                        ?>
                                    <?php endforeach ?>

                                </div>
                            </td>

                        <?php endfor ?>
                    </tr>
                <?php endfor ?>
            </table>
        </div>
    </div>

<?php Pjax::end(); ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <?= Yii::t('goal', 'Calendar chart') ?>

            </h3>
        </div>
        <div class="panel-body">
            <?= ChartJs::widget([
                'type' => 'Line',
                'options' => [
                    'height' => 80,
                    'width' => '100%'
                ],
                'data' => $dataForChart
            ]);
            ?>
        </div>
    </div>

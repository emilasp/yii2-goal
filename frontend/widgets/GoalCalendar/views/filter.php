<?php
use emilasp\core\extensions\PjaxFilter\PjaxFilter;
use emilasp\goal\common\models\Goal;
use emilasp\goal\common\models\GoalDirection;
use emilasp\goal\common\models\GoalProject;
use emilasp\variety\models\Variety;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<div class="goal-calendar-filter well">
    <h3><?= Yii::t('goal', 'Goal Filter') ?>:</h3>

    <div>
        <div class="position-relative">
            <label class="clearfix"><?= Yii::t('goal', 'Goal result type') ?></label>

            <?= PjaxFilter::widget([
                'type'      => PjaxFilter::TYPE_SELECT,
                'id'        => 'status',
                'items'     => (new Goal)->statuses,
                'selectors' => [
                    'container' => ['.goal-calendar-filter'],
                    'reload'    => ['#goal-calendar'],
                    'exclude'   => [],
                ],
            ]) ?>
        </div>

        <div class="position-relative">
            <label class="clearfix"><?= Yii::t('goal', 'Goal result type') ?></label>

            <?= PjaxFilter::widget([
                'type'      => PjaxFilter::TYPE_SELECT,
                'id'        => 'result_status',
                'items'     => Variety::getValues('bool'),
                'selectors' => [
                    'container' => ['.goal-calendar-filter'],
                    'reload'    => ['#goal-calendar'],
                    'exclude'   => [],
                ],
            ]) ?>
        </div>


        <div class="position-relative">
            <label class="clearfix"><?= Yii::t('goal', 'Goal type units') ?></label>

            <?= PjaxFilter::widget([
                'type'      => PjaxFilter::TYPE_SELECT,
                'id'        => 'type_unit',
                'items'     => (new Goal)->type_units,
                'selectors' => [
                    'container' => ['.goal-calendar-filter'],
                    'reload'    => ['#goal-calendar'],
                    'exclude'   => [],
                ],
            ]) ?>
        </div>

        <div class="position-relative">
            <label class="clearfix"><?= Yii::t('goal', 'Goal directions') ?></label>

            <?= PjaxFilter::widget([
                'id'        => 'direction_id',
                'items'     =>
                    GoalDirection::find()->where(PjaxFilter::getFilterData('filters', ['id' => 'direction_id']))->byCreatedBy()->byStatus()->map()->all(),
                'selectors' => [
                    'container' => ['.goal-calendar-filter'],
                    'reload'    => ['#pjax-filter-project_id', '#pjax-filter-id', '#goal-calendar'],
                    'exclude'   => ['project_id', 'id'],
                ],
            ]) ?>
        </div>

        <div class="position-relative">
            <label><?= Yii::t('goal', 'Goal projects') ?></label>
            <?= PjaxFilter::widget([
                'id'        => 'project_id',
                'items'     =>
                    GoalProject::find()->where(PjaxFilter::getFilterData('filters', [
                        'id'           => 'project_id',
                        'direction_id' => 'direction_id',
                    ]))->byCreatedBy()->byStatus()->map()->all(),
                'selectors' => [
                    'container' => ['.goal-calendar-filter'],
                    'reload'    => ['#pjax-filter-id', '#goal-calendar'],
                    'exclude'   => ['id'],
                ],
            ]) ?>
        </div>

        <div class="position-relative">
            <label><?= Yii::t('goal', 'Goal goals') ?></label>
            <?= PjaxFilter::widget([
                'id'        => 'id',
                'items'     =>
                    Goal::find()->where(PjaxFilter::getFilterData('filters', [
                        'id'           => 'id',
                        'project_id'   => 'project_id',
                        'direction_id' => 'direction_id',
                    ]))->byCreatedBy()->byStatus()->map()->all(),
                'selectors' => [
                    'container' => ['.goal-calendar-filter'], // Селекторы блока с фильтрами
                    'reload'    => ['#goal-calendar'], // Селекторы блоков для обновления
                    'exclude'   => [], // Селекторы фильтров которые нужно исключить из обновления
                ],
            ]) ?>
        </div>
    </div>
</div>

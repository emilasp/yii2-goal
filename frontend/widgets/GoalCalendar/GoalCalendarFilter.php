<?php
namespace emilasp\goal\frontend\widgets\GoalCalendar;

use DateTime;
use emilasp\core\components\base\Widget;
use emilasp\core\helpers\DateHelper;
use emilasp\goal\common\models\Goal;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class GoalCalendarFilter
 * @package emilasp\goal\frontend\widgets\GoalCalendar
 */
class GoalCalendarFilter extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        echo $this->render('filter');
    }

}

<?php

namespace emilasp\goal\frontend\widgets\GoalCalendar;

use emilasp\core\components\base\AssetBundle;

/**
 * Class GoalCalendar
 * @package emilasp\goal\frontend\widgets\GoalCalendarAsset
 */
class GoalCalendarAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'calendar'
    ];
    public $js = [
        'calendar'
    ];
}

<?php
namespace emilasp\goal\frontend\widgets\GoalCalendar;

use DateTime;
use emilasp\core\components\base\Widget;
use emilasp\core\extensions\PjaxFilter\PjaxFilter;
use emilasp\core\helpers\DateHelper;
use emilasp\goal\common\models\Goal;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class GoalCalendar
 * @package emilasp\goal\frontend\widgets\GoalCalendar
 */
class GoalCalendar extends Widget
{
    public $date = 'now';

    private $prevMonth;
    private $curentMonth;
    private $nextMonth;

    private $firstDayMonth;
    private $lastDayMonth;

    private $dateStart;
    private $dateEnd;

    private $datesWithGoals         = [];
    private $datesWithGoalsForChart = [];
    private $allGoalsId = [];


    public function init()
    {
        $this->registerAssets();
        $this->setMonths();

    }

    public function run()
    {
        $dates = $this->getDaysByLastMondey($this->date);
        $this->fillDatesGoals($dates);
        echo $this->render('calendar', [
            'data'         => $this->datesWithGoals,
            'dataForChart' => $this->datesWithGoalsForChart,
            'dateStart'    => $this->firstDayMonth,
            'dateEnd'      => $this->lastDayMonth,
            'prevMonth'    => $this->prevMonth,
            'curentMonth'  => $this->curentMonth,
            'nextMonth'    => $this->nextMonth,
            'nowDate'      => date('Y-m-d'),
        ]);
    }

    /**
     * Устанавливаем месяца
     */
    private function setMonths()
    {
        $this->date        = Yii::$app->request->get('date', 'now');
        $this->curentMonth = (new DateTime($this->date))->format('M y');
        $this->prevMonth   = (new DateTime($this->date))->modify('-1 months')->format('Y-m-01');
        $this->nextMonth   = (new DateTime($this->date))->modify('+1 months')->format('Y-m-01');
    }

    /** Заполняем цели по датам
     *
     * @param [] $dates
     *
     * @return Goal[]
     */
    private function fillDatesGoals(array $dates)
    {
        $attrs   = [
            'id'            => 'id',
            'result_status' => 'result_status',
            'type_unit'     => 'type_unit',
            'project_id'    => 'project_id',
            'direction_id'  => 'direction_id',
        ];
        $filters = PjaxFilter::getFilterData('filters', $attrs);

        $resultChart = [
            'axes' => [],
            'data' => [],
        ];

        foreach ($dates as $date) {
            $goals = Goal::getGoalsByDate($date);

            $resultChart['axes'][] = date('d', strtotime($date));

            $indexCart = count($resultChart['axes']) - 1;

            foreach ($resultChart['data'] as $goalId => $goalChartData) {
                $resultChart['data'][$goalId][$indexCart] = 0;
            }

            $resultGoals = [];
            foreach ($goals as $goal) {
                $resultGoal = $goal->getResultOfDay($date);

                ///////////////////
                if (!isset($resultChart['data'][$goal->id])) {
                    $resultChart['data'][$goal->id] = [];

                    foreach ($resultChart['axes'] as $day) {
                        $resultChart['data'][$goal->id][] = 0;
                    }
                }

                if ($resultGoal && $resultGoal->result) {
                    $resultChart['data'][$goal->id][$indexCart] = $resultGoal->result;
                }

                ///////////////////

                $add = true;

                foreach ($attrs as $attr) {
                    if ($attr === 'result_status' && !empty($filters[$attr])) {

                        if (!$resultGoal || !in_array($resultGoal->status, $filters[$attr])) {
                            $add = false;
                        }
                    } elseif (!empty($filters[$attr]) && !in_array($goal->{$attr}, $filters[$attr])) {
                        $add = false;
                    }
                }

                if ($add) {
                    $resultGoals[] = $goal;
                }
            }

            $this->datesWithGoals[] = [
                'date'  => $date,
                'goals' => $resultGoals,
            ];
        }


        $this->datesWithGoalsForChart['labels'] = $resultChart['axes'];
        foreach ($resultChart['data'] as $goalItem => $data) {
            $this->datesWithGoalsForChart['datasets'][] = [
                'fillColor' => "rgba(220,220,220,0.5)",
                'strokeColor' => '#' . strtoupper(dechex(random_int(0,10000000))),
                'pointColor' => '#' . strtoupper(dechex(random_int(0,10000000))),
                'pointStrokeColor' => "#fff",
                'data' => $data
            ];
        }
    }

    /** Возвращаем даты с полными неделями на начало и окончание месяца
     *
     * @param string $date
     *
     * @return array
     */
    private function getDaysByLastMondey(string $date = 'now')
    {
        $this->dateStart     = (new DateTime($date))->modify('first day of this month');
        $this->firstDayMonth = $this->dateStart->format('Y-m-d');
        $this->dateStart     = $this->dateStart->modify('Monday this week')->modify('+1 days')->format('Y-m-d');

        $this->dateEnd      = (new DateTime($date))->modify('last day of this month');
        $this->lastDayMonth = $this->dateEnd->format('Y-m-d');
        $this->dateEnd      = $this->dateEnd->modify('Sunday this week')->modify('+1 days')->format('Y-m-d');


        return DateHelper::getDatesByRange($this->dateStart, $this->dateEnd);
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        GoalCalendarAsset::register($view);
        $view->params['sidebar']['left'] .= GoalCalendarFilter::widget();
    }
}

<?php
namespace emilasp\goal\frontend\widgets\GoalSidebar;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use yii;

/**
 * Class GoalSidebar
 * @package emilasp\goal\frontend\widgets\GoalSidebar
 */
class GoalSidebar extends Widget
{
    public $curDay;
    public function init()
    {
        $this->registerAssets();
        $this->curDay = date('Y-m-d');
    }

    public function run()
    {
        echo $this->render('info', [
            'now' => $this->curDay,
            'data' => $this->getDataForInfo()
        ]);

        echo $this->render('menu', [
            'now' => $this->curDay,
        ]);
    }

    /** Собираем статистику по дню
     * @return array
     */
    private function getDataForInfo()
    {
        $models = Goal::getGoalsByDate($this->curDay);

        $resultDay = [
            'new'  => 0,
            'good' => 0,
            'fail' => 0,
        ];

        foreach ($models as $model) {
            $resultOfDay = $model->getResultOfDay($this->curDay);

            if (!$resultOfDay || !$resultOfDay->id) {
                $resultDay['new']++;
            } elseif ($resultOfDay->status) {
                $resultDay['good']++;
            } else {
                $resultDay['fail']++;
            }
        }
        return $resultDay;
    }


    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        GoalSidebarAsset::register($view);
    }
}

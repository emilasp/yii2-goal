<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="well sidebar">
    <h3>Menu</h3>
    <a href="<?= Url::toRoute('/goal/goal-dashboard') ?>" class="sidebar-link">
        <?= Yii::t('goal', 'My day') ?>
    </a>
    <a href="<?= Url::toRoute('/goal/goal-dashboard/calendar') ?>" class="sidebar-link">
        <?= Yii::t('goal', 'Calendar') ?>
    </a>
    <hr />
    <a href="<?= Url::toRoute('/goal/goal-direction') ?>" class="sidebar-link">
        <?= Yii::t('goal', 'Directions') ?>
    </a>
    <a href="<?= Url::toRoute('/goal/goal-project') ?>" class="sidebar-link">
        <?= Yii::t('goal', 'Projects') ?>
    </a>
    <a href="<?= Url::toRoute('/goal/goal') ?>" class="sidebar-link">
        <?= Yii::t('goal', 'Goals') ?>
    </a>
</div>
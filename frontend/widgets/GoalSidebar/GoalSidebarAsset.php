<?php

namespace emilasp\goal\frontend\widgets\GoalSidebar;

use emilasp\core\components\base\AssetBundle;

/**
 * Class GoalSidebarAsset
 * @package emilasp\goal\frontend\widgets\GoalSidebar
 */
class GoalSidebarAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'sidebar'
    ];
    public $js = [
        //'sidebar'
    ];
}

<?php
namespace emilasp\goal\frontend\widgets\GoalFormPeriod;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\GoalPeriod;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

/**
 * Class GoalFormPeriod
 * @package emilasp\goal\frontend\widgets\GoalFormPeriod
 */
class GoalFormPeriod extends Widget
{
    /** @var  ActiveForm */
    public $form;
    public $model;
    public $attributeWeek = 'formPeriods[' . GoalPeriod::TYPE_DAY_OF_WEEK . ']';
    public $attributeMonth = 'formPeriods[' . GoalPeriod::TYPE_DAY_OF_MONTH . ']';

    public function init()
    {
        $this->registerAssets();
        $this->model->setFormPeriods();
    }

    public function run()
    {
        echo Html::beginTag('div', ['class' => 'clearfix']);
        echo $this->form->field($this->model, $this->attributeWeek)->checkboxList(
            [
                1 => 'Пн',
                2 => 'Вт',
                3 => 'Ср',
                4 => 'Чт',
                5 => 'Пт',
                6 => 'Сб',
                7 => 'Вс',
            ],
            [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $class = 'day-of-week';
                    if ($index >= 5) {
                        $class = 'day-of-week-red';
                    }
                    return Html::checkbox($name, $checked, [
                        'value' => $value,
                        'label' => $label,
                        'labelOptions' => ['class' => $class . ' ripple-effect'],
                    ]);
                },
            ]
        );
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => 'clearfix']);
        echo $this->form->field($this->model, $this->attributeMonth)->checkboxList(
            [
                '1'  => 1,
                '2'  => 2,
                '3'  => 3,
                '4'  => 4,
                '5'  => 5,
                '6'  => 6,
                '7'  => 7,
                '8'  => 8,
                '9'  => 9,
                '10' => 10,
                '11' => 11,
                '12' => 12,
                '13' => 13,
                '14' => 14,
                '15' => 15,
                '16' => 16,
                '17' => 17,
                '18' => 18,
                '19' => 19,
                '20' => 20,
                '21' => 21,
                '22' => 22,
                '23' => 23,
                '24' => 24,
                '25' => 25,
                '26' => 26,
                '27' => 27,
                '28' => 28,
                '29' => 29,
                '30' => 30,
                '31' => 31,
            ],
            [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $class = 'day-of-week';
                    if (in_array($index, [10, 20])) {
                        $class = 'day-of-week-br';
                    }
                    return Html::checkbox($name, $checked, [
                        'value' => $value,
                        'label' => $label,
                        'labelOptions' => ['class' => $class . ' ripple-effect'],
                    ]);
                },
            ]
        )->label(Yii::t('goal', 'Period days of month'));
        echo Html::endTag('div');

        /*echo $this->render('input', [
            'form'      => $this->form,
            'model'     => $this->model,
            'attribute' => $this->attribute,
            'periods'   => Json::decode($this->model->{$this->attribute}),
        ]);*/
    }


    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        GoalFormPeriodAsset::register($view);
    }
}

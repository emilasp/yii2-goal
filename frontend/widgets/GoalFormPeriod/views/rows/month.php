<?php
use yii\helpers\Url;
?>
<div class="dash-day-row-goal">
    <div class="dash-day-goal-checked">
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-success">O</button>
            </div>
            <div class="col-md-6">
                <button class="btn btn-danger">X</button>
            </div>
        </div>
    </div>
    <div class="dash-day-goal-img">
        <img src="" height="30px" />
    </div>
    <div class="dash-day-goal-name">
        <?= $model['name'] ?>
    </div>
    <div class="dash-day-goal-description">
        <button class="btn btn-info">I</button>
    </div>
</div>
<?php

namespace emilasp\goal\frontend\widgets\GoalFormPeriod;

use emilasp\core\components\base\AssetBundle;

/**
 * Class GoalFormPeriod
 * @package emilasp\goal\frontend\widgets\GoalFormPeriodAsset
 */
class GoalFormPeriodAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'list'
    ];
    public $js = [
        'list'
    ];
}

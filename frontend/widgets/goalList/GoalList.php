<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\goal\frontend\widgets\goalList;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use emilasp\goal\common\models\GoalDirection;
use emilasp\goal\common\models\GoalProject;
use yii;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

class GoalList extends Widget
{
    const TYPE_DIRECTION = 'direction';
    const TYPE_PROJECT   = 'project';
    const TYPE_GOAL      = 'goal';

    public $type      = self::TYPE_DIRECTION;
    public $pageSizes = 10;

    private $dataProvider;

    public function init()
    {
        $this->registerAssets();

        $this->getDataProvider();
    }

    public function run()
    {
        $type = $this->type;
        echo ListView::widget([
            'dataProvider' => $this->dataProvider,
            'itemOptions'  => ['class' => 'item'],
            'itemView'     => function ($model, $key, $index, $widget) use ($type) {
                return $this->render($type, ['model' => $model, 'index' => $index]);
            },
        ]);
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        GoalListAsset::register($view);
    }

    private function getDataProvider()
    {
        $query = null;
        switch ($this->type) {
            case self::TYPE_DIRECTION:
                $query = GoalDirection::find();
                break;
            case self::TYPE_PROJECT:
                $query = GoalProject::find();
                break;
            case self::TYPE_GOAL:
                $query = Goal::find();
                break;
        }

        $this->dataProvider = new ActiveDataProvider([
            'query'      => $query
                ->where([
                    'created_by' => Yii::$app->user->id
                ])
                ->orderBy('status, name'),
            'pagination' => [
                'pageSize' => $this->pageSizes,
            ],
        ]);
    }
}

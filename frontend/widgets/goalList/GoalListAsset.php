<?php

namespace emilasp\goal\frontend\widgets\goalList;

use emilasp\core\components\base\AssetBundle;

/**
 * Class GoalListAsset
 * @package emilasp\goal\frontend\widgets\goalList
 */
class GoalListAsset extends AssetBundle
{
    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        //$this->setupAssets('js', ['list']);
        //$this->setupAssets('css', ['list']);
        parent::init();
    }


}

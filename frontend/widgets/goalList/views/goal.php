<?php
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-3"><?= $model->id ?></div>
    <div class="col-md-3">
        <a href="<?= Url::toRoute(['/goal/goal/update', 'id' => $model->id]) ?>">
            <?= $model->name ?>
        </a>
    </div>
    <div class="col-md-3"><?= $model->status ?></div>
</div>
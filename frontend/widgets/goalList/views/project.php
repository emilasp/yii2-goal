<?php
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-3"><?= $model->id ?></div>
    <div class="col-md-3">
        <a href="<?= Url::toRoute(['/goal/goal-project/update', 'id' => $model->id]) ?>">
            <?= $model->name ?>
        </a>
    </div>
    <div class="col-md-3"><?= count($model->goals) ?></div>
</div>
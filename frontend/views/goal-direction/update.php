<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\goal\common\models\GoalDirection */

$this->title = Yii::t('goal', 'Update {modelClass}: ', [
    'modelClass' => 'Goal Direction',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('goal', 'Goal Directions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('goal', 'Update');
?>
<div class="goal-direction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

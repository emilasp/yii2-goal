<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\goal\common\models\GoalDirection */

$this->title = Yii::t('goal', 'Create Goal Direction');
$this->params['breadcrumbs'][] = ['label' => Yii::t('goal', 'Goal Directions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goal-direction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

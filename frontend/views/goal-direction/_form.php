<?php

use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use emilasp\files\extensions\imageInputWidget\ImageInputWidget;

/* @var $this yii\web\View */
/* @var $model emilasp\goal\common\models\GoalDirection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goal-direction-form well">

    <?php $form = ActiveForm::begin([
        'id'      => 'goal-direction-form-id',
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= ImageInputWidget::widget([
                'form'      => $form,
                'model'     => $model,
            ]) ?>

            <?php if (Yii::$app->user->can('admin')) : ?>
            <?= $form->field($model, 'standart')->dropDownList($model->standarts) ?>
            <?php endif ?>

            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                'options' => ['rows' => 1],
                'preset'  => 'standart',
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('goal', 'Create') : Yii::t('goal', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

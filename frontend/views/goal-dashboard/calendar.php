<?php

use emilasp\goal\frontend\widgets\DashDayGoals\DashDayGoalsWeek;
use emilasp\goal\frontend\widgets\GoalCalendar\GoalCalendar;
use yii\helpers\Html;
use emilasp\goal\frontend\widgets\DashDayGoals\DashDayGoals;
use emilasp\goal\frontend\widgets\DashDayGoals\DashDayTimeline;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\goal\common\models\search\GoalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('goal', 'Calendar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GoalCalendar::widget() ?>

</div>

<?php

use emilasp\goal\frontend\widgets\DashDayGoals\DashDayGoalsWeek;
use yii\helpers\Html;
use emilasp\goal\frontend\widgets\DashDayGoals\DashDayGoals;
use emilasp\goal\frontend\widgets\DashDayGoals\DashDayTimeline;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\goal\common\models\search\GoalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('goal', 'Goals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DashDayGoals::widget() ?>

    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">Дни недели</h3></div>
        <div class="panel-body">
            <?= DashDayTimeline::widget() ?>
            <?= DashDayGoalsWeek::widget() ?>
        </div>
    </div>

</div>

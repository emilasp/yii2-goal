<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\goal\common\models\GoalProject */

$this->title = Yii::t('goal', 'Create Goal Project');
$this->params['breadcrumbs'][] = ['label' => Yii::t('goal', 'Goal Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goal-project-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

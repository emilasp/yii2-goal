<?php

use dosamigos\ckeditor\CKEditor;
use emilasp\files\extensions\imageInputWidget\ImageInputWidget;
use emilasp\goal\common\models\GoalDirection;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\goal\common\models\GoalProject */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goal-project-form well">

    <?php $form = ActiveForm::begin([
        'id'      => 'goal-project-form-id',
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'direction_id')->dropDownList(
                ArrayHelper::map(GoalDirection::find()->byCreatedBy()->all(), 'id', 'name')
            ) ?>

            <?= ImageInputWidget::widget([
                'form'      => $form,
                'model'     => $model,
            ]) ?>

            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                'options' => ['rows' => 1],
                'preset'  => 'standart',
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('goal', 'Create') : Yii::t('goal', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\goal\common\models\Goal */

$this->title = Yii::t('goal', 'Create Goal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('goal', 'Goals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
use emilasp\files\extensions\FileManager\FileManagerForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model emilasp\goal\common\models\Goal */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="goal-form well">

        <?php $form = ActiveForm::begin(); ?>

        <?php echo $form->errorSummary($model); ?>

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('goal', 'Tab base') ?></a></li>
            <li><a data-toggle="tab" href="#settings"><?= Yii::t('goal', 'Tab settings') ?></a></li>
            <li><a data-toggle="tab" href="#images"><?= Yii::t('goal', 'Tab images') ?></a></li>
        </ul>

        <div class="tab-content">
            <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
            <?= $this->render('tabs/_settings', ['form' => $form, 'model' => $model]) ?>
            <?= $this->render('tabs/_images', ['form' => $form, 'model' => $model]) ?>
        </div>




        <div class="form-group">
            <?= Html::submitButton(
                $model->isNewRecord ? Yii::t('goal', 'Create') : Yii::t('goal', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
            ) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?= FileManagerForm::widget(['model' => $model]) ?>
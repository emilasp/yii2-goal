<?php
use dosamigos\ckeditor\CKEditor;
use emilasp\goal\common\models\GoalDirection;
use emilasp\goal\common\models\GoalProject;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \emilasp\goal\common\models\Goal */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="base" class="tab-pane fade in active clearfix">

    <h2><?= Yii::t('goal', 'Base goal settings') ?></h2>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'direction_id')->dropDownList(
                ArrayHelper::map(GoalDirection::find()->byCreatedBy()->all(), 'id', 'name')
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'project_id')->dropDownList(
                ArrayHelper::map(GoalProject::find()->byCreatedBy()->all(), 'id', 'name')
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList($model->types) ?>
        </div>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'public')->checkbox() ?>
        </div>
    </div>

</div>
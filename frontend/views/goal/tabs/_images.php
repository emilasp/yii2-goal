<?php
use emilasp\files\extensions\FileManager\FileManager;

/* @var $this yii\web\View */
/* @var $model \emilasp\goal\common\models\Goal */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="images" class="tab-pane fade clearfix">

    <h2><?= Yii::t('goal', 'Images') ?></h2>

    <?= FileManager::widget(['model' => $model]) ?>

</div>
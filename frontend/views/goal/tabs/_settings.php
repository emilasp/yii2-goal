<?php
/* @var $this yii\web\View */
use emilasp\goal\common\models\Goal;
use emilasp\goal\frontend\widgets\GoalFormPeriod\GoalFormPeriod;
use kartik\widgets\TimePicker;
use yii\jui\DatePicker;
use yii\web\JsExpression;

/* @var $model Goal */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="settings" class="tab-pane fade clearfix">

    <h2><?= Yii::t('goal', 'Settings') ?></h2>

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'date_start')->widget(DatePicker::classname(), [
                        'options'    => ['class' => 'form-control'],
                        //'language' => 'ru',
                        'dateFormat' => 'yyyy-MM-dd',
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'date_end')->widget(DatePicker::classname(), [
                        'options'    => ['class' => 'form-control'],
                        //'language' => 'ru',
                        'dateFormat' => 'yyyy-MM-dd',
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'time')->widget(TimePicker::classname(), [
                        'pluginOptions' => [
                            'showSeconds'  => true,
                            'showMeridian' => false,
                            'minuteStep'   => 1,
                            'secondStep'   => 5,
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'result_expect')->input('number', ['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'type_unit')->dropDownList($model->type_units) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?= GoalFormPeriod::widget([
                'form'  => $form,
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>

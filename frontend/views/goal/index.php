<?php

use emilasp\goal\common\models\GoalDirection;
use emilasp\goal\common\models\GoalProject;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\goal\common\models\search\GoalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('goal', 'Goals');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="goal-index well">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('goal', 'Create Goal'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'           => 'image',
                'value'               => function ($model) {
                    $image = $model->getImage();
                    if ($image) {
                        return Html::img($image->getUrl('ico'), ['width' => '25px']);
                    }
                    return '-';
                },
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '30px',
                'format'               => 'raw',
            ],
            'id',
            'name',
            'text:ntext',
            [
                'attribute' => 'direction_id',
                'value'     => function ($model) {
                    return $model->direction->name;
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => ArrayHelper::map(GoalDirection::find()->byCreatedBy()->all(), 'id', 'name'),
            ],
            [
                'attribute' => 'project_id',
                'value'     => function ($model) {
                    return $model->project ? $model->project->name : null;
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => ArrayHelper::map(GoalProject::find()->byCreatedBy()->all(), 'id', 'name'),
            ],
            [
                'attribute' => 'type',
                'value'     => function ($model) {
                    return $model->types[$model->type];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => $searchModel->types,
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model) {
                    return $model->statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => $searchModel->statuses,
            ],
            // 'image_id',
            // 'unit',
            // 'date_start',
            // 'date_end',
            // 'time',
            // 'type',
            // 'type_unit',
            // 'type_period',
            // 'week',
            // 'days',
            // 'months',
            // 'public',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php

namespace emilasp\goal\frontend\controllers;

use emilasp\goal\frontend\widgets\GoalSidebar\GoalSidebar;
use Yii;
use emilasp\goal\common\models\Goal;
use emilasp\goal\common\models\search\GoalSearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GoalDashboardController implements the CRUD actions for Goal model.
 */
class GoalDashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index', 'calendar'],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                            'calendar'
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Calendar Goal models.
     * @return mixed
     */
    public function actionCalendar()
    {
        return $this->render('calendar');
    }

    /** Настраиваем сайдбар
     * @param \yii\base\Action $action
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->view->params['sidebar']['left'] = GoalSidebar::widget();
            return true;
        }
    }
}

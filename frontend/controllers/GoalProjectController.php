<?php

namespace emilasp\goal\frontend\controllers;

use emilasp\goal\frontend\widgets\GoalSidebar\GoalSidebar;
use Yii;
use emilasp\goal\common\models\GoalProject;
use emilasp\goal\common\models\search\GoalProjectSearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GoalProjectController implements the CRUD actions for GoalProject model.
 */
class GoalProjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index', 'view', 'create', 'update', 'delete'],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all GoalProject models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GoalProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GoalProject model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, GoalProject::className()),
        ]);
    }

    /**
     * Creates a new GoalProject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GoalProject();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GoalProject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, GoalProject::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GoalProject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, GoalProject::className())->delete();

        return $this->redirect(['index']);
    }

    /** Настраиваем сайдбар
     * @param \yii\base\Action $action
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->view->params['sidebar']['left'] = GoalSidebar::widget();
            return true;
        }
    }
}

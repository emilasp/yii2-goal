<?php
namespace emilasp\goal\frontend;

use emilasp\core\CoreModule;
use emilasp\settings\behaviors\SettingsBehavior;
use emilasp\settings\models\Setting;
use yii\helpers\ArrayHelper;

/**
 * Class GoalModule
 * @package emilasp\goal\frontend
 */
class GoalModule extends CoreModule
{
    public function init()
    {
        parent::init();

    }
}

<?php

namespace emilasp\goal\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "goal_period".
 *
 * @property integer $id
 * @property integer $goal_id
 * @property integer $type
 * @property integer $value
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 *
 * @property Goal $goal
 */
class GoalPeriod extends \emilasp\core\components\base\ActiveRecord
{
    const DEFAULT_RANGE_MONTHS = 3;

    const TYPE_DAY_OF_WEEK  = 1;
    const TYPE_DAY_OF_MONTH = 2;
    const TYPE_MONTH_IN_YAR = 3;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_type' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'goal_period_type',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ]
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goals_period';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goal_id', 'type', 'value'], 'required'],
            [['goal_id', 'created_by', 'type', 'value'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [
                ['goal_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Goal::className(),
                'targetAttribute' => ['goal_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('goal', 'ID'),
            'goal_id'    => Yii::t('goal', 'Goal ID'),
            'type'       => Yii::t('goal', 'Type'),
            'value'      => Yii::t('goal', 'Value'),
            'created_by' => Yii::t('goal', 'Created B'),
            'created_at' => Yii::t('goal', 'Created At'),
            'updated_at' => Yii::t('goal', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoal()
    {
        return $this->hasOne(Goal::className(), ['id' => 'goal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     *
     * @return bool|null|string
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        TagDependency::invalidate(Yii::$app->cache, Yii::$app->user->identity->getCacheTag(Goal::className()));
    }

    /** Добавляем новый период
     *
     * @param $goal_id
     * @param $type
     * @param $value
     *
     * @return bool
     */
    static public function addPeriod($goalId, $type, $value)
    {
        $result = true;
        if (is_array($value)) {
            foreach ($value as $toLink) {
                $newPeriod          = new GoalPeriod();
                $newPeriod->goal_id = $goalId;
                $newPeriod->type    = $type;
                $newPeriod->value   = $toLink;
                if (!$newPeriod->save()) {
                    $result = false;
                }
            }
        } else {
            $newPeriod          = new GoalPeriod();
            $newPeriod->goal_id = $goalId;
            $newPeriod->type    = $type;
            $newPeriod->value   = $value;
            $result             = $newPeriod->save();
        }
        return $result;
    }
}

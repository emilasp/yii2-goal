<?php

namespace emilasp\goal\common\models;

use emilasp\files\behaviors\FileSingleBehavior;
use emilasp\files\models\File;
use emilasp\goal\common\models\query\DirectionActiveQuery;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "goal_direction".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $image_id
 * @property integer $standart
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property File $image
 * @property User $createdBy
 * @property User $updatedBy
 * @property Goal[] $goalGoals
 */
class GoalDirection extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_standart' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'standart',
                'group'     => 'bool',
            ],
            [
                'class' => FileSingleBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goals_direction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['text'], 'string'],
            [['image_id', 'status', 'created_by', 'updated_by', 'standart'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['image_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => File::className(),
                'targetAttribute' => ['image_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('goal', 'ID'),
            'name'       => Yii::t('goal', 'Name'),
            'text'       => Yii::t('goal', 'Text'),
            'image_id'   => Yii::t('goal', 'Image ID'),
            'standart'   => Yii::t('goal', 'Standart'),
            'status'     => Yii::t('goal', 'Status'),
            'created_at' => Yii::t('goal', 'Created At'),
            'updated_at' => Yii::t('goal', 'Updated At'),
            'created_by' => Yii::t('goal', 'Created By'),
            'updated_by' => Yii::t('goal', 'Updated By'),
        ];
    }

    /**
     * @return DirectionActiveQuery
     */
    public static function find()
    {
        return new DirectionActiveQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoalGoals()
    {
        return $this->hasMany(Goal::className(), ['direction_id' => 'id']);
    }
}

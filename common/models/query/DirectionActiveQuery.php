<?php
namespace emilasp\goal\common\models\query;

use emilasp\core\components\base\BaseActiveQuery;
use Yii;

/**
 * Class DirectionActiveQuery
 * @package emilasp\goal\common\models\query
 */
class DirectionActiveQuery extends BaseActiveQuery
{
    /**
     * @param null $id
     *
     * @return $this
     */
    public function byCreatedBy($id = null)
    {
        if (!$id && !Yii::$app->user->isGuest) {
            $id = Yii::$app->user->id;
        }
        return $this->andWhere(['OR', ['created_by' => $id], ['standart' => 1]]);
    }
}

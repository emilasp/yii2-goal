<?php

namespace emilasp\goal\common\models;

use DateTime;
use emilasp\core\helpers\DateHelper;
use emilasp\files\behaviors\FileBehavior;
use emilasp\files\models\File;
use emilasp\seo\common\behaviors\SeoModelBehavior;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\DbDependency;
use yii\caching\TagDependency;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "goal_goal".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $direction_id
 * @property integer $project_id
 * @property integer $image_id
 * @property integer $type_unit
 * @property string $unit
 * @property integer $unit_step
 * @property integer $result_expect
 * @property integer $result_actual
 * @property string $date_start
 * @property string $date_end
 * @property string $time
 * @property integer $type
 * @property integer $type_period
 * @property integer $week
 * @property integer $days
 * @property integer $months
 * @property integer $public
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property File $image
 * @property GoalDirection $direction
 * @property GoalProject $project
 * @property User $createdBy
 * @property User $updatedBy
 */
class Goal extends \emilasp\core\components\base\ActiveRecord
{
    const STATUS_FAIL = 0;
    const STATUS_END = 1;
    const STATUS_JOB = 2;
    const STATUS_PAUSE = 3;

    const UNIT_TYPE_SIMPLE = 1;
    const UNIT_TYPE_TIME   = 2;
    const UNIT_TYPE_CUSTOM = 3;

    static public $periodTypes = [
        GoalPeriod::TYPE_DAY_OF_WEEK,
        GoalPeriod::TYPE_DAY_OF_MONTH,
    ];

    public $formPeriods;

    private $statistics;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'files'          => [
                'class' => FileBehavior::className(),
            ],
            'variety_type'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'goal_type',
            ],
            'variety_unit'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type_unit',
                'group'     => 'goal_unit',
            ],
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'goal_status',
            ],
            [
                'class'           => SeoModelBehavior::className(),
                'attributeUnique' => 'id',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->unit_step = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goals_goal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'direction_id', 'result_expect', 'type', 'type_unit', 'public', 'status'],
                'required',
            ],
            [['text'], 'string'],
            [
                [
                    'direction_id',
                    'project_id',
                    'type',
                    'type_unit',
                    'unit_step',
                    'result_expect',
                    'result_actual',
                    'public',
                    'status',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            ['formPeriods', 'checkPeriods'],
            [['date_start', 'date_end', 'time', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 30],
            [['unit'], 'string', 'max' => 15],
            [
                ['direction_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => GoalDirection::className(),
                'targetAttribute' => ['direction_id' => 'id'],
            ],
            [
                ['project_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => GoalProject::className(),
                'targetAttribute' => ['project_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],

            ['unit_step', 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('goal', 'ID'),
            'name'          => Yii::t('goal', 'Name'),
            'text'          => Yii::t('goal', 'Text'),
            'direction_id'  => Yii::t('goal', 'Direction ID'),
            'project_id'    => Yii::t('goal', 'Project ID'),
            'unit'          => Yii::t('goal', 'Unit'),
            'unit_step'     => Yii::t('goal', 'Unit step'),
            'date_start'    => Yii::t('goal', 'Date Start'),
            'date_end'      => Yii::t('goal', 'Date End'),
            'time'          => Yii::t('goal', 'Time'),
            'type'          => Yii::t('goal', 'Type'),
            'type_unit'     => Yii::t('goal', 'Type Unit'),
            'result_expect' => Yii::t('goal', 'Result in dey expect'),
            'result_actual' => Yii::t('goal', 'Result actual'),
            'public'        => Yii::t('goal', 'Public'),
            'status'        => Yii::t('goal', 'Status'),
            'created_at'    => Yii::t('goal', 'Created At'),
            'updated_at'    => Yii::t('goal', 'Updated At'),
            'created_by'    => Yii::t('goal', 'Created By'),
            'updated_by'    => Yii::t('goal', 'Updated By'),
            'formPeriods'   => Yii::t('goal', 'Periods'),
        ];
    }


    /** Проверяем выбран ли хотя бы один период дял цели
     *
     * @param $attribute
     * @param $params
     */
    public function checkPeriods($attribute, $params)
    {
        $isPeriods = false;
        foreach ($this->formPeriods as $type) {
            if (is_array($type) && count($type)) {
                $isPeriods = true;
            }
        }
        if (!$isPeriods) {
            $this->addError($attribute, Yii::t('goal', 'Select periods for goal'));
        }
    }


    /** Возвращаем результат за день
     *
     * @param null $date
     *
     * @return null
     */
    public function getResult($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }
        foreach ($this->results as $result) {
            if (date('Y-m-d', strtotime($result->created_at)) === $date) {
                return $result;
            }
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(GoalResult::className(), ['goal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriods()
    {
        return $this->hasMany(GoalPeriod::className(), ['goal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirection()
    {
        return $this->hasOne(GoalDirection::className(), ['id' => 'direction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(GoalProject::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }



    /**
     * @param bool $insert
     *
     * @return bool|null|string
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->date_start = $this->date_start ? $this->date_start : date('Y-m-d H:i:s');

            $this->setUnitStep();
            $this->caclFactResult();
            return true;
        }
        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     *
     * @return bool|null|string
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->savePeriods();

        TagDependency::invalidate(Yii::$app->cache, Yii::$app->user->identity->getCacheTag(self::className()));
    }

    public function afterDelete()
    {
        parent::afterDelete();
        //$this->deleteFiles();
    }



    /**
     * Сохраняем новые периоды и удаляем старые
     */
    private function savePeriods()
    {
        $toLinkWeek   = [];
        $toUnLinkWeek = [];
        /** Добавляем новые периоды */
        foreach (self::$periodTypes as $type) {
            if (isset($this->formPeriods[$type])) {
                $toLinkWeek = (array)$this->formPeriods[$type];
                foreach ($this->periods as $modelPeriod) {
                    if ($modelPeriod->type === $type) {
                        $keyWeek = array_search($modelPeriod->value, $toLinkWeek);
                        if ($keyWeek !== false) {
                            unset($toLinkWeek[$keyWeek]);
                        } else {
                            $toUnLinkWeek[] = $modelPeriod->id;
                        }
                    }
                }
            }
            GoalPeriod::addPeriod($this->id, $type, $toLinkWeek);
            GoalPeriod::deleteAll(['id' => $toUnLinkWeek]);
        }
    }

    /**
     * Заполняем периоды для атрибута формы formPeriods
     */
    public function setFormPeriods()
    {
        foreach ($this->periods as $period) {
            $this->formPeriods[$period->type][] = $period->value;
        }
    }

    /**
     * Устанавливаем шаг
     */
    private function setUnitStep()
    {
        $this->unit_step = ceil($this->result_expect / 10);
    }


    /** Получаем цели на дату
     *
     * @param null|string $date
     *
     * @return Goal[]
     */
    public static function getGoalsByDate(string $date = null) : array
    {
        $typeWeek  = GoalPeriod::TYPE_DAY_OF_WEEK;
        $typeMonth = GoalPeriod::TYPE_DAY_OF_MONTH;
        $createdBy = Yii::$app->user->id;
        $date      = $date ? $date : date('Y-m-d H:i:s');

        $sql   = <<<SQL
        SELECT goal.* FROM goals_goal goal
          LEFT JOIN goals_period p_week ON p_week.goal_id=goal.id AND p_week.type={$typeWeek}
          LEFT JOIN goals_period p_month ON p_month.goal_id=goal.id AND p_month.type={$typeMonth}
          LEFT JOIN goals_result result
            ON result.goal_id=goal.id AND result.created_at::date='{$date}'::date
        WHERE goal.created_by={$createdBy}

              AND (goal.date_start IS NULL OR goal.date_start<='{$date} 23:59:59')
              AND (goal.date_end IS NULL OR goal.date_end>='{$date} 00:00:00')

              AND (
                    EXTRACT(ISODOW FROM TIMESTAMP '{$date}') = p_week.value
                    OR EXTRACT(DAY FROM TIMESTAMP '{$date}') = p_month.value
                  )
        GROUP BY goal.id
        HAVING max(p_week.id) IS NOT NULL OR max(p_month.id) IS NOT NULL
        ORDER BY max(result.status) DESC
SQL;
        $query = self::findBySql($sql)->with('results');
        $dependency = new TagDependency(['tags' => Yii::$app->user->identity->getCacheTag(self::className())]);

        $result = self::getDb()->cache(function ($db) use ($query) {
            return $query->all();
        }, 3600, $dependency);

        return $result;
    }

    /** Проверяем есть ли цель на дату
     *
     * @param $date
     *
     * @return mixed
     */
    public function isGoalInDate($date)
    {
        $dates = $this->getDatesWithGoal(null, $date);
        return array_shift($dates);
    }

    /** Получаем даты для периода
     *
     * @param GoalPeriod[] $periods
     * @param bool $date - за определённую дату
     * @param bool $excludeNoEventDays - включить даты без совпадений периодов
     * @param bool $beforeNow - исключить будущие даты
     *
     * @return array
     */
    public function getDatesWithGoal($periods = null, $date = null, $excludeNoEventDays = false, $beforeNow = false)
    {
        $dateStart = $this->date_start ?? date('Y-m-d H:i:s');
        $dateEnd = $this->date_end ?? date('Y-m-d H:i:s', strtotime('+' . GoalPeriod::DEFAULT_RANGE_MONTHS . ' month'));

        if ($beforeNow) {
            $dates = DateHelper::getDatesByRange($dateStart, date('Y-m-d H:i:s'));
        } elseif (!$date) {
            $dates = DateHelper::getDatesByRange($dateStart, $dateEnd);
        } else {
            $dates = DateHelper::getDatesByRange($date, date('Y-m-d H:i:s', strtotime($date . ' +1 days')));
        }

        if (!$periods) {
            $periods = $this->periods;
        }

        $events = [];
        foreach ($dates as $date) {
            $dateObj  = new \DateTime($date);
            $weekDay  = (int)$dateObj->format('N');
            $monthDay = (int)$dateObj->format('d');

            if (!$excludeNoEventDays) {
                $events[$date] = false;
            }
            foreach ($periods as $period) {
                if ($period->type === GoalPeriod::TYPE_DAY_OF_WEEK && $weekDay === $period->value) {
                    $events[$date] = true;
                } elseif ($period->type === GoalPeriod::TYPE_DAY_OF_MONTH && $monthDay === $period->value) {
                    $events[$date] = true;
                }
            }
        }
        return $events;
    }


    /**
     * Получаем будущие события(этапы - дата, ожидаемый результат)
     */
    public function getFutureStages()
    {
        $stages  = [];
        $dateEnd = $this->date_end;
        if (!$dateEnd) {
            $dateEnd = date('Y-m-d', strtotime("+92 days"));
        }

        if (!$this->periods) {
            return $stages;
        }


        foreach ($this->periods as $period) {
            $stages = ArrayHelper::merge($stages, $this->getDatesByPeriod($period));
        }
    }


    /** Получаем результат дня для Цели
     *
     * @param $day
     *
     * @return null
     */
    public function getResultOfDay($day)
    {
        $result = null;
        foreach ($this->results as $resultGoal) {
            if (date('Y-m-d', strtotime($resultGoal->created_at)) === date('Y-m-d', strtotime($day))) {
                $result = $resultGoal;
            }
        }
        if (!$result && $day < date('Y-m-d')) {
            $result         = new GoalResult();
            $result->status = GoalResult::STATUS_DISABLED;
            $result->text   = Yii::t('goal', 'Result overdue');
        }
        return $result;
    }

    /** Получаем общую статистику по Цели
     * @return array
     */
    public function getResultStatistic()
    {
        if (!$this->statistics) {
            $dates = $this->getDatesWithGoal(null, null, false);

            $formatResults = [];
            foreach ($this->results as $result) {
                $formatResults[date('Y-m-d', strtotime($result->created_at))] = $result;
            }

            $dateNow = date('Y-m-d');

            $countDays          = 0;
            $countResults       = 0;
            $summResults        = 0;
            $summResultsExpect  = 0;
            $countResultsExpect = 0;
            $countResultsFuture = 0;
            $countResultsFail   = 0;
            $countContinues     = 0;
            $countMaxContinues  = 0;
            foreach ($dates as $date => $isGoal) {
                $countDays++;
                if ($isGoal) {
                    $countResultsExpect++;
                    $summResultsExpect += $this->result_expect;
                    if (isset($formatResults[$date])) {
                        if ($formatResults[$date]->status === GoalResult::STATUS_ENABLED) {
                            $summResults += $formatResults[$date]->result;
                            $countResults++;
                            $countContinues++;
                        } else {
                            $countContinues = 0;
                            $countResultsFail++;
                        }
                        if ($countMaxContinues < $countContinues) {
                            $countMaxContinues = $countContinues;
                        }
                    }
                    if ($dateNow < $date) {
                        $countResultsFuture++;
                    }
                }
            }
            $this->statistics = [];
            $this->statistics = [
                'days'              => $countDays, // Дней
                'results'           => $countResults, // Фактически результатов
                'expects'           => $countResultsExpect, // Всего ожидается результатов
                'future'            => $countResultsFuture, // Оставшиеся результаты
                'fails'             => $countResultsFail, // Завернутые результаты
                'continue'          => $countContinues, // Последняяя безостановочная
                'maxContinue'       => $countMaxContinues, // Максимум без фейлов
                'percent'           => $summResults / ($summResultsExpect / 100), // Процент выполнения по суммам
                'percentCount'      => $countResults / ($countResultsExpect / 100), // Процент выполнения по суммам
                'summResults'       => $summResults, // Фактический результат (сумма)
                'summResultsExpect' => $summResultsExpect, //  Ожидаемый результат (сумма)
            ];
        }
        return $this->statistics;
    }

    /** Получаем все результаты по цели
     * @return array
     */
    public function getAllResultsByDate()
    {
        $dates = $this->getDatesWithGoal(null, null, false, true);

        $formatResults = [];
        foreach ($this->results as $result) {
            $formatResults[date('Y-m-d', strtotime($result->created_at))] = $result;
        }

        $data = [];
        foreach ($dates as $date => $isGoal) {
            if ($isGoal) {
                if (isset($formatResults[$date])) {
                    if ($formatResults[$date]->status === GoalResult::STATUS_ENABLED) {
                        $data[$date] = $formatResults[$date]->result;
                    } else {
                        $data[$date] = 0;
                    }
                } else {
                    $data[$date] = 0;
                }
            }
        }
        ksort($data);
        return $data;
    }

    /**
     * Считаем фактический результат(из GoalResult)
     */
    private function caclFactResult()
    {
        $this->result_actual = 0;
        foreach ($this->results as $result) {
            $this->result_actual += $result->result;
        }
    }
}

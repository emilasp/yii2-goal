<?php

namespace emilasp\goal\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "goal_result".
 *
 * @property integer $id
 * @property integer $goal_id
 * @property integer $result
 * @property string $text
 * @property integer $status
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $createdBy
 * @property Goal $goal
 */
class GoalResult extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ]
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goals_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goal_id', 'result', 'status'], 'required'],
            [['goal_id', 'result', 'status', 'created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['text'], 'string', 'max' => 255],
            [
                ['goal_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Goal::className(),
                'targetAttribute' => ['goal_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('goal', 'ID'),
            'goal_id'    => Yii::t('goal', 'Goal ID'),
            'result'     => Yii::t('goal', 'Result'),
            'text'       => Yii::t('goal', 'Text'),
            'status'     => Yii::t('goal', 'Status'),
            'created_by' => Yii::t('goal', 'Created By'),
            'created_at' => Yii::t('goal', 'Created At'),
            'updated_at' => Yii::t('goal', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoal()
    {
        return $this->hasOne(Goal::className(), ['id' => 'goal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     *
     * @return bool|null|string
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        TagDependency::invalidate(Yii::$app->cache, Yii::$app->user->identity->getCacheTag(Goal::className()));

        $this->saveGoal();
    }

    /**
     * Сохраняем новый результат для цели(пересчет)
     */
    public function saveGoal()
    {
        $this->goal->markAttributeDirty('result_actual');
        $this->goal->save();
    }

    /** Добавляем результат
     * @param $day
     * @param $goalId
     * @param $status
     * @param $value
     * @param $comment
     *
     * @return bool
     */
    public static function addResult($day, $goalId, $status, $value, $comment)
    {
        $findSql = <<<SQL
        SELECT * FROM goals_result
        WHERE goal_id={$goalId}
        AND DATE(created_at) = '{$day}'
SQL;
        $result = self::findBySql($findSql)->one();

        if (!$result) {
            $result = new self();
        }

        if (!$status) {
            $value = 0;
        } elseif (!$value) {
            $value = 1;
        }

        $result->goal_id = $goalId;
        $result->status = $status;
        $result->result = $value;
        $result->text = $comment;
        return $result->save();
    }
}

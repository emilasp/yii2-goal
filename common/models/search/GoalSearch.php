<?php

namespace emilasp\goal\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\goal\common\models\Goal;

/**
 * GoalSearch represents the model behind the search form about `emilasp\goal\common\models\Goal`.
 */
class GoalSearch extends Goal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'direction_id',
                    'project_id',
                    'type',
                    'type_unit',
                    'unit_step',
                    'result_expect',
                    'result_actual',
                    'public',
                    'status',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['name', 'text', 'unit', 'date_start', 'date_end', 'time', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Goal::find();
        $query->with('results');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'            => $this->id,
            'direction_id'  => $this->direction_id,
            'project_id'    => $this->project_id,
            'date_start'    => $this->date_start,
            'date_end'      => $this->date_end,
            'time'          => $this->time,
            'type'          => $this->type,
            'type_unit'     => $this->type_unit,
            'result_expect' => $this->result_expect,
            'result_actual' => $this->result_actual,
            'unit_step'     => $this->unit_step,
            'public'        => $this->public,
            'status'        => $this->status,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'created_by'    => $this->created_by,
            'updated_by'    => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'text', $this->text])
              ->andFilterWhere(['like', 'unit', $this->unit]);

        return $dataProvider;
    }
}

<?php
namespace emilasp\site\common\extensions\FlashMsg;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class FlashMsg
 * @package emilasp\site\common\extensions\FlashMsg
 */
class FlashMsg extends Widget
{
    public static $typeColors = [
        'success' => 'green',
        'info'    => 'grey',
        'primary' => 'blue',
        'warning' => 'orange',
        'danger'  => 'red',
    ];

    public function run()
    {
        $js = '';
        $messages = Yii::$app->session->getAllFlashes();
        $delayOpen = 0;
        foreach ($messages as $type => $message) {
            $js .= '
            setTimeout(function(){
                new jBox("Notice", {
                        "id":"notice'.$delayOpen.'",
                        "content":"' . $message . '",
                        "position":{"x":"right","y":"top"},
                        "color": "' . self::$typeColors[$type] . '",
                        "animation":"tada"
                    });
            },
            '.$delayOpen.');
            ';
            $delayOpen += 1000;
        }
        $this->view->registerJs($js);
    }
}

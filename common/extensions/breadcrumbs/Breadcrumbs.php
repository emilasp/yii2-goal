<?php
namespace emilasp\site\common\extensions\breadcrumbs;

use yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * Расширение для вывода хлебных крошек
 *
 * Class Breadcrumbs
 * @package emilasp\site\common\extensions\breadcrumbs
 */
class Breadcrumbs extends \yii\base\Widget
{
    public $first;
    public $flat;

    public $items;

    public function init()
    {
        $homeLink = '/';

        $this->items = ArrayHelper::merge([
            'first' => [
                'url'   => Url::toRoute($homeLink),
                'label' => Yii::t('site', 'Home'),
            ],
        ], $this->items);
        $this->registerAssets();
    }

    public function run()
    {
        echo $this->render('breadcrumbs', ['items' => $this->items]);
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        BreadcrumbsAsset::register($view);
    }
}

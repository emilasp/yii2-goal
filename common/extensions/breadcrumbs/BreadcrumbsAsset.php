<?php
namespace emilasp\site\common\extensions\breadcrumbs;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class BreadcrumbsAsset
 * @package emilasp\site\extensions\breadcrumbs
 */
class BreadcrumbsAsset extends AssetBundle
{
    public $jsOptions = ['position' => View::POS_HEAD];

    public $sourcePath = __DIR__ . '/assets';

    public $css = ['breadcrumbs.css'];

}

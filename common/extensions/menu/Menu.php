<?php
namespace emilasp\site\common\extensions\menu;

use yii;
use yii\helpers\Url;
use yii\base\Widget;

/**
 * Class TopMenu
 * @package emilasp\site\extensions\menu
 */
class Menu extends Widget
{
    const TYPE_HORIZONTAL = 'horizontal';
    const TYPE_VERTICAL   = 'vertical';
    const CACHE_PREFIX    = 'user_menu:';

    public $menuPath = '@app/config/menu/';
    public $menuName = 'menu';
    public $menus    = [];

    public $type = self::TYPE_HORIZONTAL;

    private $action;
    private $controller;
    private $module;
    private $userId;

    private $decompositionUrl;

    public function init()
    {
        $this->registerAssets();

        $this->action     = Yii::$app->controller->action->id;
        $this->controller = Yii::$app->controller->id;
        $this->module     = Yii::$app->controller->module->id;
        $this->userId    = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : null;
    }

    public function run()
    {
        $items = include(Yii::getAlias($this->menuPath . $this->menuName . '.php'));
        $items = $this->removeByRight($items);

        //$items = $this-> setActiveItems( $items );

        $menu = [['options' => ['class' => 'navbar-nav'], 'items' => $items]];

        echo $this->render('menu-' . $this->type, ['menus' => $menu]);
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        MenuAsset::register($view);
    }


    /** Убираем из списка меню все элементы не проходящие по правам
     *
     * @param $items
     *
     * @return mixed
     */
    private function removeByRight($items)
    {
        foreach ($items as $index => $item) {
            $items[$index]['active'] = $this->isActive($items[$index]['url']);

            if (is_array($item)) {
                if (isset($item['items'])) {
                    $items[$index]['items'] = $this->removeByRight($item['items']);

                    if (count($items[$index]['items']) == 0) {
                        unset($items[$index]);
                    }
                } else {
                    if (isset($item['role'])) {
                        $isAllowRole = false;
                        foreach ((array)$item['role'] as $role) {
                            if (Yii::$app->user->can($role)) {
                                $isAllowRole = true;
                            }
                        }
                        if (!$isAllowRole) {
                            unset($items[$index]);
                        }
                    }

                    if (isset($item['rule']) && is_callable($item['rule'])) {
                        $rule = $item['rule'];
                        if ($rule() !== true) {
                            unset($items[$index]);
                        }
                    }
                }
            }
        }
        return $items;
    }

    private function isActive($route)
    {
        $route = $this->decompositionRoute($route);

        if (Yii::$app->controller->module->id === $route['module']) {
            if (Yii::$app->controller->id === $route['controller'] || $route['controller'] === null) {
                if (Yii::$app->controller->action->id === $route['action'] || !$route['action']) {
                    return true;
                }
            }
        }
        return false;
    }

    /** Получаем наименования модуля, контроллера и экшена
     *
     * @param $route
     *
     * @return array
     */
    private function decompositionRoute($route)
    {
        if (!$this->decompositionUrl) {
            $data = [
                'module'     => null,
                'controller' => null,
                'action'     => null,
            ];

            if (strpos($route, '/') !== false) {
                $route = substr($route, 1);
            }
            $dataRoute = explode('/', $route);
            $count     = count($dataRoute);
            for ($i = 0; $i < $count; $i++) {
                switch ($i) {
                    case 0:
                        $data['module'] = $dataRoute[$i];
                        break;
                    case 1:
                        $data['controller'] = $dataRoute[$i];
                        break;
                    case 2:
                        $data['action'] = $dataRoute[$i];
                        break;
                }
            }
            $this->decompositionUrl = $data;
        }

        return $this->decompositionUrl;
    }


    /** Рекурсивно выставляем активные пункты меню
     *
     * @param $items
     *
     * @return mixed
     */
    /* private function setActiveItems( $items ){
         foreach( $items as $index=>$item ){
             if(is_array($item)){
                 if(isset($item['items'])){
                     $items[$index]['items'] = $this->setActiveItems($item['items']);
                 }else{
                     $action = Yii::$app->rights->getNameAction( $this->action, $this->controller, $this->module );

                     foreach($items[$index]['role'] as $role){
                         if( $action==$role ){
                             $items[$index]['active'] = true;
                             break;
                         }
                     }
                 }
             }
         }
         return $items;
     }*/
}

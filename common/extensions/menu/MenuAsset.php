<?php
namespace emilasp\site\common\extensions\menu;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class MenuAsset
 * @package emilasp\site\common\extensions\menu
 */
class MenuAsset extends AssetBundle
{
    public $jsOptions = ['position' => View::POS_END];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $sourcePath = '@vendor/emilasp/yii2-site/common/extensions/menu/assets';

    public $css = [
        'tmenu.css',
    ];

    public $js = [];
}
